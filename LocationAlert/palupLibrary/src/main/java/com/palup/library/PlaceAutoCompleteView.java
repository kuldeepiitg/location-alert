package com.palup.library;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.palup.library.adapter.PlaceAutocompleteAdapter;

/**
 * Place suggestion view.
 * <p/>
 * AutoCompleteTextView is used for general purpose autocompletion.
 * This view suggests places from google map. Use just like a AutoCompleteTextView
 * <p/>
 * Created by kuldeep on 16/06/16.
 */
public class PlaceAutoCompleteView extends AutoCompleteTextView {

    /**
     * Latitude and longitude bound for place search.
     * FIXME: currently covering only India, set relevant bounds
     */
    private static final LatLngBounds PLACE_BOUND = new LatLngBounds(
            new LatLng(6.0, 68.0),
            new LatLng(35.0, 98.0));

    /**
     * Logging tag.
     */
    private static final String LOG_TAG = "Place Chooser";

    /**
     * Google API client
     */
    private GoogleApiClient googleApiClient;

    /**
     * Place autocomplete adapter
     */
    private PlaceAutocompleteAdapter placeAutocompleteAdapter;

    /**
     * Place selection listener.
     */
    private OnPlaceSelectionListener onPlaceSelectionListener;

    /**
     * Selected place.
     */
    private Place place;

    public PlaceAutoCompleteView(Context context) {
        super(context);
        init();
    }

    public PlaceAutoCompleteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PlaceAutoCompleteView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public PlaceAutoCompleteView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    /**
     * Initialize.
     */
    private void init() {
        PlaceOnConnectionFailedListener onConnectionFailedListener = new PlaceOnConnectionFailedListener();
        this.googleApiClient = new GoogleApiClient.Builder(getContext()).
                enableAutoManage((FragmentActivity) getContext(), 0 /* clientId */, onConnectionFailedListener).
                addApi(Places.GEO_DATA_API).
                build();

        placeAutocompleteAdapter = new PlaceAutocompleteAdapter(getContext(), googleApiClient, PLACE_BOUND, null);
        setAdapter(placeAutocompleteAdapter);

        AdapterView.OnItemClickListener placeAutoCompleteClickListener = new PlaceAutoCompleteClickListener();
        setOnItemClickListener(placeAutoCompleteClickListener);
    }

    /**
     * Get selected place.
     *
     * @return the selected place.
     */
    public Place getPlace() {
        return place;
    }

    /**
     * Set OnPlaceSelectionListener for the AutoCompleteView
     *
     * @param onPlaceSelectionListener listener to be set.
     */
    public void setOnPlaceSelectionListener(OnPlaceSelectionListener onPlaceSelectionListener) {
        this.onPlaceSelectionListener = onPlaceSelectionListener;
    }

    /**
     * Hide soft keyboard
     */
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindowToken(), 0);
    }

    /**
     * Place selection listener.
     */
    public interface OnPlaceSelectionListener {

        /**
         * Execute on place selection,
         * <p/>
         * The method will be called when user taps on one of the selection.
         */
        void onPlaceSelection(Place place);
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(
     *com.google.android.gms.common.api.GoogleApiClient, String...)
     */
    public class PlaceAutoCompleteClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = placeAutocompleteAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            // Issue a request to the Places Geo Data API to retrieve a Place object with additional
            // details about the place.
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(googleApiClient, placeId);

            ResultCallback<? super PlaceBuffer> placeDetailsCallback = new PlaceResultCallback();
            placeResult.setResultCallback(placeDetailsCallback);

            Toast.makeText(getContext(), "Clicked: " + primaryText,
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private class PlaceResultCallback implements ResultCallback<PlaceBuffer> {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            place = places.get(0);
            if (onPlaceSelectionListener != null) {
                onPlaceSelectionListener.onPlaceSelection(place);
            }
            hideKeyboard();
            places.release();
        }
    }

    /**
     * OnConnectionFailedListener for place
     */
    private class PlaceOnConnectionFailedListener implements GoogleApiClient.OnConnectionFailedListener {

        /**
         * Called when the Activity could not connect to Google Play services and the auto manager
         * could resolve the error automatically.
         * In this case the API is not available and notify the user.
         *
         * @param connectionResult can be inspected to determine the cause of the failure
         */
        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            Log.e(LOG_TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = "
                    + connectionResult.getErrorCode());
        }
    }
}
