package com.palup.library.fragment;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.palup.library.R;

/**
 * The reminder dialog
 * <p/>
 * Created by Kuldeep on 17-Jun-16.
 */
public class ReminderDialog extends DialogFragment {

    /**
     * Listener on dismissal/close of dialog
     */
    private OnDismissListener onDismissListener;

    /**
     * Root view of the dialog layout
     */
    private View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_reminder, container, false);

        return rootView;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            String text = (String) ((TextView) (rootView.findViewById(R.id.reminder))).getText();
            onDismissListener.onDismiss(text);
        }
    }

    /**
     * Set OnDismissListener.
     *
     * @param onDismissListener listener instance to set
     */
    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    /**
     * Listener on dialog dismissal
     */
    public interface OnDismissListener {

        /**
         * Execute on dialog dismissal
         */
        void onDismiss(String reminder);
    }
}
