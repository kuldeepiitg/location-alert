package com.palup.locationalert;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.palup.library.PlaceAutoCompleteView;
import com.palup.library.fragment.ReminderDialog;

/**
 * Home screen, place chooser.
 */
public class PlaceChooser extends AppCompatActivity implements OnMapReadyCallback {

    /**
     * Log tag
     */
    private String LOG_TAG = "Place Chooser";

    /**
     * Google map to get/set place by markers
     */
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_chooser);

        initializeMap();
        initializePlaceAutoCompleteView();
    }

    /**
     * Initialize the google map
     */
    private void initializeMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Initialize place autocomplete
     */
    private void initializePlaceAutoCompleteView() {
        PlaceAutoCompleteView placeAutoCompleteView = (PlaceAutoCompleteView) findViewById(R.id.place);
        assert placeAutoCompleteView != null;
        placeAutoCompleteView.setOnPlaceSelectionListener(new PlaceAutoCompleteView.OnPlaceSelectionListener() {
            @Override
            public void onPlaceSelection(Place place) {
                googleMap.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()).draggable(true));

                CameraPosition cameraPosition = new CameraPosition.Builder().target(
                        place.getLatLng()).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeMap();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnInfoWindowLongClickListener(new GoogleMap.OnInfoWindowLongClickListener() {
            @Override
            public void onInfoWindowLongClick(final Marker marker) {
                marker.hideInfoWindow();
                ReminderDialog reminderDialog = new ReminderDialog();
                reminderDialog.show(getFragmentManager(), "Message");
                reminderDialog.setOnDismissListener(new ReminderDialog.OnDismissListener() {
                    @Override
                    public void onDismiss(String reminder) {
                        Log.i(LOG_TAG, reminder);
                        marker.setSnippet(reminder);
                    }
                });
            }
        });
    }
}
